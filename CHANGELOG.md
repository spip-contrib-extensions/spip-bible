# Changelog

## [6.0.1] 2022-06-09

### Added

- Compatibilite SPIP 4.1

### Fix

- Compatiblité PHP 8.0
- Valeur par défaut des configs de traduction en fr, de
- Suppression des configs de pl, fi, ru
