<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_bible_saisies_dist() {
	include_spip('inc/bible_tableau');
	$saisies = [];

	$trad_defaut = [];
	$trad_pp = [];
	$cfg_trad_propre = lire_config('bible/pp/trad_prop');

	foreach(bible_tableau('separateur') as $lang => $sep) {
		$traductions = bible_traductions($lang, true);
		$data = [];
		$trad_pp_cases = [];
		foreach ($traductions as $trad) {
			$data[$trad] = info_bible_version($trad, 'traduction');
			$trad_pp_cases[] = [
				'saisie' => 'case',
				'options' => [
					'nom' => 'pp[trad_prop][]',
					'valeur_oui' => $trad,
					'id' => "trad_prop_$trad",
					'valeur_forcee' => in_array($trad, $cfg_trad_propre ?? array()) ? $trad : '',
					'conteneur_class' => 'pleine_largeur',
					'label_case' => $data[$trad],
				]
			];
		}

		$trad_defaut[] = [
			'saisie' => 'selection',
			'options' => [
				'nom' => "traduction_$lang",
				'label' => _T("bible:cfg_traduction_$lang"),
				'data' => $data,
				'conteneur_class' => 'pleine_largeur',
				'cacher_option_intro' => true
			]
		];


		$trad_pp[] = [
			'saisie' => 'fieldset',
			'options' => [
				'label' => ucfirst(_T('bible:cfg_pp_version_par_lang', ['lang' => traduire_nom_langue($lang)])),
				'nom' => 'trad_prop_'.$lang,
				'conteneur_class' => 'version',
			],
			'saisies' => $trad_pp_cases,
		];
	}

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'trad_defaut',
			'id_saisie' => 'trad_defaut',
			'label' => _T('bible:cfg_trad_defauts'),
			'onglet' => true,
		],
		'saisies' => $trad_defaut
	];


	$alias = [];
	foreach (bible_tableau('alias') as $al => $description) {
		$data = [];
		foreach ($description['options'] as $o) {
			$data[$o] = $o;
		}
		$alias[] = [
			'saisie' => 'selection',
			'options' => [
				'nom' => "alias_$al",
				'label' => $al,
				'data' => $data,
				'cacher_option_intro' => true,
				'conteneur_class' => 'pleine_largeur',
			]
		];
	}

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'alias_version',
			'id_saisie' => 'alias_version',
			'label' => _T('bible:cfg_alias_version'),
			'onglet' => true,
		],
		'saisies' => $alias
	];



	$formes_livres = [];
	foreach	(array_keys(find_all_in_path('forme_livre/', '')) as $forme) {
		$forme = basename($forme, '.html');
		$formes_livres[$forme] = _T("bible:cfg_forme_livre_$forme");
	}

	$polices = [];
	foreach (bible_tableau('police') as $lang => $choix_police) {
		$data = [];
		foreach ($choix_police as $police) {
			$data[$police] = $police;
		}
		$polices[] = [
			'saisie' => 'selection',
			'options' => [
				'nom' => "police_$lang",
				'label' => _T("bible:police_$lang"),
				'data' => $data,
				'conteneur_class' => 'pleine_largeur',
				'option_intro' => _T('item_non'),
			]
		];
	}

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'affichage',
			'id_saisie' => 'affichage',
			'label' => _T('bible:cfg_affichage'),
			'onglet' => true,
		],
		'saisies' => array_merge([
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'numeros',
					'label_case' => _T('bible:cfg_numeros'),
					'conteneur_class' => 'pleine_largeur',
					'valeur_oui' => 'oui',
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'retour',
					'label_case' => _T('bible:cfg_retour'),
					'conteneur_class' => 'pleine_largeur',
					'valeur_oui' => 'oui',
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'ref',
					'label_case' => _T('bible:cfg_ref'),
					'conteneur_class' => 'pleine_largeur',
					'valeur_oui' => 'oui',
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'nommer_trad',
					'label_case' => _T('bible:cfg_nommer_trad'),
					'conteneur_class' => 'pleine_largeur',
					'valeur_oui' => 'oui',
				],
			],
			[
				'saisie' => 'case',
				'options' => [
					'nom' => 'url',
					'label_case' => _T('bible:cfg_url'),
					'conteneur_class' => 'pleine_largeur',
					'valeur_oui' => 'oui',
				],
			],
			[
				'saisie' => 'selection',
				'options' => [
					'nom' => 'forme_livre',
					'label' => _T('bible:cfg_forme_livre'),
					'data' => $formes_livres,
					'conteneur_class' => 'pleine_largeur',
					'cacher_option_intro' => true,
				],
			],
		],
		$polices
		)
	];

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'pp',
			'id_saisie' => 'pp',
			'label' => _T('bible:cfg_pp_label'),
			'onglet' => true,
		],
		'saisies' => array_merge([
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'pp_choix_laisses',
					'label' => _T('bible:cfg_pp_choix_laisses')
				],
				'saisies' => [
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/numeros',
							'label_case' => _T('bible:cfg_pp_numeros'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/retour',
							'label_case' => _T('bible:cfg_pp_retour'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/ref',
							'label_case' => _T('bible:cfg_pp_ref'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/nommer_trad',
							'label_case' => _T('bible:cfg_pp_nommer_trad'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/forme_livre',
							'label_case' => _T('bible:cfg_pp_forme_livre'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/url',
							'label_case' => _T('bible:cfg_pp_url'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/lang_pas_art',
							'label_case' => _T('bible:cfg_pp_lang_pas_art'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
					[
						'saisie' => 'case',
						'options' =>  [
							'nom' => 'pp/lang_morte',
							'label_case' => _T('bible:cfg_pp_lang_morte'),
							'conteneur_class' => 'pleine_largeur',
							'valeur_oui' => 'oui',
						]
					],
				]
			],
		],
		$trad_pp
		),
	];
	$saisies['options'] = [
		'inserer_debut' => '
    <script type="text/javascript">
    // <![CDATA[

    $(document).ready(function() {
        $(".version legend").after(\'<p><a class="coche" href="#">Tout cocher</a> | <a class="decoche" href="#">Tout decocher</a> </p>\');
        $("a.decoche").click(function() {
                $(this).parents("fieldset").first().find(":checkbox").prop("checked",false);
                return false;
        });
        $("a.coche").click(function() {
                $(this).parents("fieldset").first().find(":checkbox").prop("checked",true);
                return false;
        });
});
    // ]]>
		</script>
			'
	];
	return $saisies;

}

