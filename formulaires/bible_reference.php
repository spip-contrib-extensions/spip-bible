<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}


include_spip('inc/config');
include_spip('inc/bible_tableau');
function formulaires_bible_reference_saisies_dist($lang='fr', $cadre='1') {
	$saisies = [];



	// Le resultat pour copier-coller
	if ($resultat = _request('bible_pp_resultat')) {
		$saisies[] = [
				'saisie' => 'textarea',
				'options' => [
					'defaut' => $resultat,
					'nom' => 'resultat',
				]
		];
	}

	$saisies[] = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'passage',
			'label' => _T('bible:form_passage')
		]
	];

	// Determiner les langues à proposer
	if (lire_config('bible/pp/lang_morte')) {
		$langues = bible_tableau('langues');
	} else {
		$langues = array_keys(bible_tableau('separateur'));
	}

	if (!lire_config('bible/pp/lang_pas_art')) {
		$langues = array_merge(
			[$lang],
			array_diff($langues, array_keys(bible_tableau('separateur')))
		);
	}
	// A partir de là, déterminer les traductions à proposer
	$traductions = array_intersect(
		bible_traductions($langues, 'oui'),
		lire_config('bible/pp/trad_prop') ?? []
	);
	$data_traduction = [];
	foreach ($traductions as $trad) {
		$lang_trad = ucfirst(info_bible_version($trad, 'lang'));
		if (!isset($data_traduction)) {
			$data_traduction[$lang_trad] = [];
		}
		$data_traduction[$lang_trad][$trad] = info_bible_version($trad, 'traduction');

	}
	$saisies[] = [
		'saisie' => 'selection',
		'options' => [
			'nom' => 'version',
			'label' => _T('bible:form_version'),
			'data' => $data_traduction,
			'cacher_option_intro' => true,
		]
	];

	// Les options sur l'affichage
	foreach (['numeros', 'retour', 'ref', 'nommer_trad', 'url'] as $r) {
		if (lire_config("bible/pp/$r")) {
			$saisies[] = [
				'saisie' => 'case',
				'options' => [
					'nom' => $r,
					'label_case' => _T("bible:form_$r"),
					'valeur_oui' => 'oui',
					'conteneur_class' => 'pleine_largeur',
				]
			];
		} else {
			$saisies[] = [
				'saisie' => 'hidden',
				'options' => [
					'nom' => $r,
					'defaut' => lire_config("bible/$r"),
				]
			];
		}
	}

	// Options pour la forme des livres
	$formes_livres = [];
	foreach	(array_keys(find_all_in_path('forme_livre/', '')) as $forme) {
		$forme = basename($forme, '.html');
		$formes_livres[$forme] = _T("bible:cfg_forme_livre_$forme");
	}
	if (lire_config('bible/pp/forme_livre')) {
		$saisies[] = [
			'saisie' => 'selection',
			'options' => [
				'nom' => 'forme_livre',
				'cacher_option_intro' => true,
				'label' => _T('bible:form_forme_livre'),
				'data' => $formes_livres,
				'defaut' => lire_config('bible/forme_livre')
			]
		];
	}



	// Réglages globales des saisies du formulaire
	$saisies['options'] = [
		'texte_submit' => _T('bible:ok'),
		'inserer_debut' => '<h2 class="titrem">'._T('bible:presse_papier_titre').'</h2>'
	];
	return $saisies;
}


function formulaires_bible_reference_charger_dist($lang='fr',$cadre=1){
	$valeurs = array(
		'cadre'=>$cadre,
	   'version'      =>  lire_config('bible/traduction_'.$lang),
	   'lang'      =>$lang,
	   'numeros'   => lire_config('bible/numeros'),
	   'retour'    => lire_config('bible/retour'),
	   'ref'       => lire_config('bible/ref'),
	   'nommer_trad' => lire_config("bible/nommer_trad"),
	   'forme_livre' => lire_config('bible/forme_livre'),
	   'url'	=> lire_config("bible/url")
	);
	return $valeurs;
}
function formulaires_bible_reference_verifier_dist($lang='fr',$cadre=1){

    $passage    = str_replace(' ','',_request('passage'));
    $version    = _request('version');
    $numeros    = _request('numeros');
    $retour     = _request('retour');
    $ref        = _request('ref');
    $nommer_trad  = _request('nommer_trad');
    $forme_livre  = _request('forme_livre');
    $url 	= _request('url');
    include_spip('bible_fonctions');
    $resultat = bible($passage,$version,true);

    if ($resultat == _T('bible:pas_livre')){
			return array('message_erreur'   =>  _T('bible:form_ref_incorrecte'));

    }

}

function formulaires_bible_reference_traiter_dist($lang='fr',$cadre=1){
    include_spip('inc/filtres');
    $passage    = str_replace(' ','',_request('passage'));
    $version    = _request('version');
    $numeros    = _request('numeros');
    $retour     = _request('retour');
    $ref        = _request('ref');
    $nommer_trad        = _request('nommer_trad');
    $forme_livre  = _request('forme_livre');
    $url  = _request('url');
    include_spip('bible_fonctions');
    include_spip('inc/utils');
		$resultat = proteger_amp(
			recuperer_fond('modeles/bible',
			array(
				'passage'=>$passage,
				'traduction'=>$version,
				'retour'=>!$retour ? 'non' : $retour,
				'numeros'=>!$numeros ? 'non' : $numeros,
				'ref'=>!$ref ? 'non' : $ref,
				'nommer_trad'=>!$nommer_trad ? 'non' : $nommer_trad,
				'url'=>!$url ? 'non' : $url,
				'forme_livre'=>$forme_livre ?  $forme_livre : 'abbr',
				'propre'=>'non')
			)
		);
		set_request('bible_pp_resultat', $resultat);
		return array(
			'message_ok'=>  $cadre ? _T('bible:presse_papier_resultat') : $resultat,
			'editable' => 'oui',
		);

}
