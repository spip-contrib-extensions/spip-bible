<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/spip-bible.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'at' => 'Ancien Testament',

	// C
	'cfg' => 'Spip-Bible',
	'cfg_affichage' => 'Affichage',
	'cfg_alias_version' => 'Alias de version',
	'cfg_configurer' => 'Configurer Spip-Bible',
	'cfg_forme_livre' => 'Forme du livre',
	'cfg_forme_livre_abbr' => 'Format mixte (conseillé)',
	'cfg_forme_livre_complete' => 'Complète',
	'cfg_forme_livre_raccourcie' => 'Raccourcie (déconseillée)',
	'cfg_nommer_trad' => 'Nommer la traduction',
	'cfg_numeros' => 'Afficher les n° de chapitres et de versets dans le corps du texte',
	'cfg_pp_choix_laisses' => 'Laisser la possibilité',
	'cfg_pp_descriptif' => 'Cette page vous permet de configurer le presse-papier de Spip-Bible. Voici comment il s’affiche avec le configuration actuelle.',
	'cfg_pp_forme_livre' => 'De choisir la forme d’affichage du livre',
	'cfg_pp_label' => 'Presse-papier',
	'cfg_pp_lang_morte' => 'De choisir une version dans une langue morte',
	'cfg_pp_lang_pas_art' => 'De choisir une version dans une langue vivante autre que celle de l’article',
	'cfg_pp_nommer_trad' => 'De choisir de nommer ou non la traduction',
	'cfg_pp_numeros' => 'D’afficher ou non les numéros',
	'cfg_pp_pas_bonux' => 'Cette configuration nécéssite le plugin "SPIP-BONUX"',
	'cfg_pp_ref' => 'D’afficher ou non les références',
	'cfg_pp_retour' => 'De mettre ou non des retours à la ligne',
	'cfg_pp_url' => 'De mettre un lien vers le site source',
	'cfg_pp_version_par_lang' => '@lang@ : traductions proposées',
	'cfg_ref' => 'Afficher la référence du passage cité',
	'cfg_retour' => 'Retours à la ligne entre les versets',
	'cfg_trad_defauts' => 'Traductions par défaut',
	'cfg_traduction_bg' => 'Bulgare',
	'cfg_traduction_da' => 'Danois',
	'cfg_traduction_de' => 'Allemand',
	'cfg_traduction_en' => 'Anglais',
	'cfg_traduction_es' => 'Espagnol',
	'cfg_traduction_fi' => 'Finnois',
	'cfg_traduction_fr' => 'Français',
	'cfg_traduction_hu' => 'Magyar',
	'cfg_traduction_it' => 'Italien',
	'cfg_traduction_nl' => 'Néerlandais',
	'cfg_traduction_no' => 'Norvégien',
	'cfg_traduction_pl' => 'Polonais',
	'cfg_traduction_pt' => 'Portugais',
	'cfg_traduction_ru' => 'Russe',
	'cfg_traduction_sv' => 'Suédois',
	'cfg_url' => 'Mettre un lien vers le site source',
	'chapitre' => 'Chapitre',

	// D
	'deutero' => 'Livres Deutérocanoniques',

	// F
	'form_forme_livre' => 'Forme du livre',
	'form_nommer_trad' => 'Nommer la traduction',
	'form_numeros' => 'Afficher numéros chap./v.',
	'form_passage' => 'Passage',
	'form_ref' => 'Mettre références',
	'form_ref_incorrecte' => 'Référence incorrecte',
	'form_retour' => 'Retour-ligne entre v.',
	'form_url' => 'Pointer vers le site d’origine',
	'form_version' => 'Version',

	// H
	'historique' => 'Historique',

	// L
	'langue' => 'Langue',
	'livres_bibles' => 'Liste des livres disponibles dans la Bible version  « @trad@ »',

	// N
	'nt' => 'Nouveau Testament',

	// O
	'ok' => 'OK',

	// P
	'pas_livre' => 'L’abréviation du livre demandé n’existe pas dans cette traduction',
	'police_hbo' => 'Faut-il imposer une police spécial pour l’hébreu ?',
	'presse_papier' => 'Taper ici le code d’appel à un passage biblique',
	'presse_papier_resultat' => 'Texte ci-dessous',
	'presse_papier_titre' => 'Presse Papier Biblique',

	// S
	'supprimer' => 'Supprimer',

	// T
	'traduction_pas_dispo' => 'La traduction de la Bible demandée n’existe pas',

	// V
	'verset' => 'Verset',
	'version_dispo' => 'Traductions de la Bible disponibles'
);
